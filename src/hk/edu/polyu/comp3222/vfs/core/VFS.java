package hk.edu.polyu.comp3222.vfs.core;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

/**
 * Created by hoctell on 26/3/2017.
 */
public class VFS {
    /**
     *the root sting
     */
    public final static String ROOT = "/";

    /**
     *the file seperator
     */
    public final static String FILE_SEPARATOR = "/";

    private HashMap<String, VirtualDisk> diskMap = new HashMap<>();
    private String workingDir = ROOT;

   /**
     * Constructor
     * @param files list of path represents path list of file in the virtual disk
     * @throws IOException if  constructing class cannot be done
     */
    public VFS(List<Path> files) throws IOException {
        for (Path file : files) {
            if (Files.notExists(file))
                throw new FileNotFoundException();
            VirtualDisk disk = new ZipVirtualDisk(file);
            diskMap.put(disk.getBaseName(), disk);
        }
    }

   /**
    * @return the HashMap of the virtual disk
    */
    public HashMap<String, VirtualDisk> getVirtualDiskMap() {
        return diskMap;
    }

  /**
   * @return the HashMap of the virtual disk
   */
    public String getWorkingDir() {
        return workingDir;
    }

    /**
     * @param path represent the  represent the source path
     * @return the string of the absolutePath
     */
    public String toAbsolutePath(final String path) {
        if (!path.startsWith(ROOT)) {
            return workingDir + path;
        }
        return path;
    }

    /**
     * @param VDName  represent the name of the virual disk
     * @return the virtual disk by the specified name
     * @throws VDNotFoundException Exception if the virtual disk is not found
     */
    public VirtualDisk getVirtualDisk(String VDName)
            throws VDNotFoundException {
        VirtualDisk disk = diskMap.get(VDName);
        if (disk == null)
            throw new VDNotFoundException(VDName);
        return disk;
    }

    /**
     * @param path  represent a path
     * @return whether the paht of the virtual disk is vilad
     * @throws VDNotFoundException Exception if the virtual disk is not found
     */
    public boolean isValidPath(final String path)
            throws VDNotFoundException {
        VirtualDisk disk = phraseVirtualDisk(path);
        String diskPath = phraseDiskPath(path);
        return disk.exist(diskPath);
    }

    /**
     * @param  path  represent a path
     * @return where disk the pth stored in
     * @throws VDNotFoundException Exception if the virtual disk is not found
     */
    public VirtualDisk phraseVirtualDisk(String path)
            throws VDNotFoundException {
        String absolutePath = toAbsolutePath(path);
        String[] parts = absolutePath.split(FILE_SEPARATOR);
        return getVirtualDisk(parts[1]);
    }

    /**
     * @param  path  represent a path
     * @return the name of the sk the path stored in
     * @throws VDNotFoundException Exception if the virtual disk is not found
     */
    public String phraseDiskPath(String path)
            throws VDNotFoundException {
        phraseVirtualDisk(path);

        String absolutePath = toAbsolutePath(path);
        String[] parts = absolutePath.split(FILE_SEPARATOR);
        List<String> strings = Arrays.asList(Arrays.copyOfRange(parts, 2, parts.length));
        return String.join(FILE_SEPARATOR, strings);
    }

    /**
     * @param  file  represent a path
     * @param maxSize represent the maximum size of the virtual disk
     * @return the maximum size of the virtual disk
     * @throws IOException Exception if IO eror occur
     */
    public ZipVirtualDisk createVirtualDisk(final Path file, final long maxSize)
            throws IOException {
        ZipVirtualDisk disk = new ZipVirtualDisk(file);
        disk.createPhysicalFile(maxSize);
        diskMap.put(disk.getBaseName(), disk);
        return disk;
    }

    /**
     * @param  disk represent the input vritual isk
     * @throws IOException Exception if IO eror occur
     */
    public void deleteVirtualDisk(final VirtualDisk disk)
            throws IOException {
        diskMap.remove(disk.getBaseName(), disk);
        disk.dispose();
    }

    /**
     * clear thr virtual disk of the system
     * @throws IOException Exception if IO eror occur
     */
    public void clearVirtualDisks()
            throws IOException {
        HashMap<String, VirtualDisk> diskMap = getVirtualDiskMap();
        Iterator<Map.Entry<String, VirtualDisk>> i = diskMap.entrySet().iterator();
        while (i.hasNext()) {
            Map.Entry<String, VirtualDisk> entry = i.next();
            VirtualDisk disk = entry.getValue();
            i.remove();
            disk.dispose();
        }
    }

    /**
     * @param importPath reprensnt the import file path
     * @param dest represent the string path of the destination
     * @throws IOException Exception if IO eror occur
     * @throws VDNotFoundException if the virtual disk is not found
     * @throws NotEnoughSpaceExpection if importing files fails as not enough space
     */
    public void importFiles(final Path importPath, final String dest)
            throws IOException, VDNotFoundException, NotEnoughSpaceExpection {
        VirtualDisk targetDisk = phraseVirtualDisk(dest);

        String targetPath = phraseDiskPath(dest);
        targetDisk.importFiles(importPath, targetPath);
    }

    /**
     * @param  src represent the stinrg of the disk
     * @param exportPath  represnt of the path of the export file
     * @throws IOException Exception if IO eror occur
     * @throws VDNotFoundException if the virtual disk is not found
     */
    public void exportFiles(final String src, final Path exportPath)
            throws IOException, VDNotFoundException {
        VirtualDisk srcDisk = phraseVirtualDisk(src);

        String srcPath = phraseDiskPath(src);
        srcDisk.exportFiles(srcPath, exportPath);
    }

    /**
     * @param path represent the stinrg of the disk
     * @throws IOException Exception if IO eror occur
     * @throws VDNotFoundException if the virtual disk is not found
     */
    public void delete(final String path)
            throws IOException, VDNotFoundException {
        VirtualDisk disk = phraseVirtualDisk(path);
        String relativePath = phraseDiskPath(path);
        disk.delete(relativePath);
    }

    /**
     * @param  src represent the stinrg of the disk
     * @param target  represnt of the target path
     * @throws IOException Exception if IO eror occur
     * @throws VDNotFoundException if the virtual disk is not found
     * @throws NotEnoughSpaceExpection if importing files fails as not enough space
     */
    public void copy(final String src, final String target)
            throws IOException, VDNotFoundException, NotEnoughSpaceExpection {
        VirtualDisk srcDisk = phraseVirtualDisk(src);
        VirtualDisk targetDisk = phraseVirtualDisk(target);

        String srcPath = phraseDiskPath(src);
        String targetPath = phraseDiskPath(target);
        if (srcDisk.equals(targetDisk)) {
            srcDisk.copy(srcPath, targetPath);
        } else {
            srcDisk.exportFiles(srcPath, targetDisk.getPath(targetPath));
        }
    }

    /**
     * @param  src represent the stinrg of the disk
     * @param target  represnt of the target path
     * @throws IOException Exception if IO eror occur
     * @throws VDNotFoundException if the virtual disk is not found
     * @throws NotEnoughSpaceExpection if importing files fails as not enough space
     */
    public void move(final String src, final String target)
            throws IOException, VDNotFoundException, NotEnoughSpaceExpection {
        copy(src, target);
        delete(src);
    }

    /**
     * @param target  represnt of the target path
     * @param newName represent the new name of the file
     * @throws IOException Exception if IO eror occur
     * @throws VDNotFoundException if the virtual disk is not found
     * @throws NotEnoughSpaceExpection if importing files fails as not enough space
     */
    public void rename(final String target, final String newName)
            throws IOException, VDNotFoundException, NotEnoughSpaceExpection {
        VirtualDisk targetDisk = phraseVirtualDisk(target);
        String targetPath = phraseDiskPath(target);
        targetDisk.rename(targetPath, newName);
    }

    /**
     * @param target  represnt of the target path
     * @throws IOException Exception if IO eror occur
     * @throws VDNotFoundException if the virtual disk is not found
     */
    public void createFile(final String target)
            throws IOException, VDNotFoundException {
        VirtualDisk targetDisk = phraseVirtualDisk(target);
        String targetPath = phraseDiskPath(target);
        if (target.endsWith(FILE_SEPARATOR)) {
            targetDisk.createDirectory(targetPath);
        } else {
            targetDisk.createFile(targetPath);
        }
    }

    /**
     * @return the list of the name
     * @throws IOException Exception if IO eror occur
     * @throws VDNotFoundException if the virtual disk is not found
     */
    public List<String> list()
            throws IOException, VDNotFoundException {
        List<String> names = new ArrayList<>();
        if (workingDir.equals(ROOT)) {
            for (Map.Entry<String, VirtualDisk> entry : getVirtualDiskMap().entrySet()) {
                VirtualDisk disk = entry.getValue();
                names.add(disk.getBaseName());
            }
        } else {
            VirtualDisk disk = phraseVirtualDisk(workingDir);
            String diskPath = phraseDiskPath(workingDir);
            List<Path> files = disk.listChildren(diskPath);
            for (Path file : files) {
                names.add(file.getFileName().toString());
            }
        }
        return names;
    }

    /**
     * @param target  represnt of the target path
     * @throws Exception if target is not a valid directory
     * @throws VDNotFoundException if the virtual disk is not found
     */
    public void changeDir(final String target)
            throws Exception, VDNotFoundException {
        if (target.equals(ROOT)) {
            workingDir = ROOT;
            return;
        }
        String absolutePath = toAbsolutePath(target);
        VirtualDisk disk = phraseVirtualDisk(absolutePath);
        String diskPath = phraseDiskPath(absolutePath);
        if (diskPath.equals("")) {
            workingDir = ROOT + disk.getBaseName() + "/";
            return;
        }
        if (disk.notExist(diskPath) || !disk.isDirectory(diskPath))
            throw new Exception("Not such a directory: "+absolutePath);
        if (!absolutePath.endsWith(FILE_SEPARATOR))
            absolutePath += FILE_SEPARATOR;
        workingDir = absolutePath;
    }

    /**
     * @param  start represent the start root
     * @param filter  represnt of the SerchFilter used
     * @param keywords represent of the keyword used in the filter
     *  @return  the list ot the search result
     * @throws IOException Exception if IO eror occur
     * @throws VDNotFoundException if the virtual disk is not found
     */
    public List<String> search(final String start, final SearchFilter filter, String[] keywords)
            throws IOException, VDNotFoundException {
        List<String> stringList = new ArrayList<>();
        if (start.equals(ROOT)) {
            for (Map.Entry<String, VirtualDisk> entry : getVirtualDiskMap().entrySet()) {
                VirtualDisk disk = entry.getValue();
                List<Path> resultList = disk.search("/", filter, keywords);
                for (Path result : resultList) {
                    stringList.add("/"+disk.getBaseName()+result.toAbsolutePath());
                }
            }
            return stringList;
        }
        VirtualDisk disk = phraseVirtualDisk(start);
        String diskPath = phraseDiskPath(start);
        List<Path> resultList = disk.search(diskPath, filter, keywords);
        for (Path result : resultList) {
            stringList.add("/"+disk.getBaseName()+result.toAbsolutePath());
        }
        return stringList;
    }

    /**
     * @throws IOException Exception if IO eror occur
     */
    public void print()
            throws IOException{
        for (Map.Entry<String, VirtualDisk> entry : getVirtualDiskMap().entrySet()) {
            VirtualDisk disk = entry.getValue();
            disk.print();
        }
    }

    /**
     * @throws IOException Exception if IO eror occur
     */
    public void close()
            throws IOException{
        for (Map.Entry<String, VirtualDisk> entry : getVirtualDiskMap().entrySet()) {
            VirtualDisk disk = entry.getValue();
            disk.close();
        }
    }
}
