package hk.edu.polyu.comp3222.vfs.core;

/**
 * Created by hoctell on 28/3/2017.
 */
public class VDNotFoundException extends Exception {
   /**
    * Constructor that accepts a message
    * @param message reprente the message input
    * */
    public VDNotFoundException(String message) {
        super(message);
    }
}
