package hk.edu.polyu.comp3222.vfs.test;

import hk.edu.polyu.comp3222.vfs.core.DeleteFileVisitor;
import hk.edu.polyu.comp3222.vfs.core.SearchFilter;
import hk.edu.polyu.comp3222.vfs.core.ZipVirtualDisk;
import org.junit.Test;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Max.
 */
public class ZipVirtualDiskTest {
    private static final String WORKING_DIR = "resources/";
    private static final String FILE_A = WORKING_DIR + "project_description.pdf";
    private static final String FILE_B = WORKING_DIR + "COMP3222_PROJECT.xml";
    private static final String DIR = WORKING_DIR + "VFS-template";
    private static final long MAX_SIZE = 10000000;

    private Path path;
    private ZipVirtualDisk vd;

    /**
     * Create a virtual disk
     * @throws Exception any exception
     */
    @org.junit.Before
    public void setUp() throws Exception {
        path = Paths.get(WORKING_DIR, "test.vd");
        vd = new ZipVirtualDisk(path);
        try {
            vd.createPhysicalFile(MAX_SIZE);
            assertTrue(Files.exists(path));
        } catch (Exception x) {
            x.printStackTrace();
        }
    }


    /**
     * Dispose the virtual disk
     */
    @org.junit.After
    public void tearDown() {
        try {
            vd.print();
            vd.dispose();
        } catch (Exception x) {
            x.printStackTrace();
        }
    }

    /**
     * Test loading config
     */
    @Test
    public void testLoadingConfig() {
        try {
            vd.close();
            vd = new ZipVirtualDisk(path);
            assertEquals(MAX_SIZE, vd.getMaxSize());
        } catch (Exception x) {
            x.printStackTrace();
        }
    }

    private void importFile(String importSrc, String path) {
        try {
            vd.importFiles(Paths.get(importSrc), path);
        } catch (Exception x) {
            x.printStackTrace();
        }
    }

    private void exportFile(String path, String exportDest) {
        try {
            vd.exportFiles(path, Paths.get(exportDest));
        } catch (Exception x) {
            x.printStackTrace();
        }
    }

    private void isSameFileContent(final Path a, final Path b) {
        byte[] aByte = null, bByte = null;
        try {
            aByte = Files.readAllBytes(a);
            bByte = Files.readAllBytes(b);
        } catch (Exception x) {
            x.printStackTrace();
        }
        assertArrayEquals(aByte, bByte);
    }

    /**
     * Test importing and exporting file
     */
    @Test
    public void testPortingFile() {
        String path = "first.pdf";
        String dest = WORKING_DIR + "out.pdf";

        importFile(FILE_A, path);
        exportFile(path, dest);
        isSameFileContent(Paths.get(FILE_A), Paths.get(dest));

        try {
            Files.delete(Paths.get(dest));
        } catch (Exception x) {
            x.printStackTrace();
        }
    }

    /**
     * Test importing and exporting directory
     */
    @Test
    public void testPortingDir() {
        String path = "/dirA/dirB";
        String dest = WORKING_DIR + "outDir/";

        importFile(DIR, path);
        exportFile(path, dest);

        try {
            Files.walkFileTree(Paths.get(dest), new DeleteFileVisitor());
        } catch (Exception x) {
            x.printStackTrace();
        }
    }

    /**
     * Test deleting file
     */
    @Test
    public void testDeletingFile() {
        String path = "deleteFile";

        importFile(FILE_B, path);
        try {
            vd.delete(path);
        } catch (Exception x) {
            x.printStackTrace();
        }
    }

    /**
     * Test deleting directory
     */
    @Test
    public void testDeletingDir() {
        String path = "/dirA/dirB/file";

        importFile(FILE_B, path);
        try {
            vd.delete("/dirA/");
        } catch (Exception x) {
            x.printStackTrace();
        }
    }

    /**
     * Test copying file
     */
    @Test
    public void testCopyingFile() {
        String from = "/from";
        String to = "/to";

        importFile(FILE_B, from);
        try {
            vd.copy(from, to);
        } catch (Exception x) {
            x.printStackTrace();
        }

        isSameFileContent(vd.getPath(from), vd.getPath(to));
    }

    /**
     * Test copying directory
     */
    @Test
    public void testCopyingDir() {
        String from = "from";
        String to = "to";

        importFile(DIR, from);
        try {
            vd.copy(from, to);
        } catch (Exception x) {
            x.printStackTrace();
        }
    }

    /**
     * Test moving file
     */
    @Test
    public void testMovingFile() {
        String from = "first.pdf";
        String to = "/dirA/dirB";

        importFile(FILE_A, from);
        try {
            vd.move(from, to);
        } catch (Exception x) {
            x.printStackTrace();
        }
    }

    /**
     * Test moving directory
     */
    @Test
    public void testMovingDir() {
        String from = "/from";
        String to = "/to/dirA";

        importFile(DIR, from);
        try {
            vd.move(from, to);
        } catch (Exception x) {
            x.printStackTrace();
        }
    }

    /**
     * Test renaming file
     */
    @Test
    public void testRenamingFile() {
        String path = "/dirA/dirB/file";
        String newName = "new";

        importFile(FILE_A, path);
        try {
            vd.rename(path, newName);
        } catch (Exception x) {
            x.printStackTrace();
        }
    }

    /**
     * Test deleting directory
     */
    @Test
    public void testRenamingDir() {
        String path = "/dir";
        String newName = "new";

        importFile(DIR, path);
        try {
            vd.rename(path, newName);
        } catch (Exception x) {
            x.printStackTrace();
        }
    }

    /**
     * Test creating directory
     */
    @Test
    public void testCreatingDir() {
        String dirPath = "/testDir/";
        try {
            vd.createDirectory(dirPath);
        } catch (Exception x) {
            x.printStackTrace();
        }
    }

    /**
     * Test listing children of a given path
     */
    @Test
    public void testListingChildren() {
        String path = "/dir";

        importFile(DIR, path);
        try {
            List<Path> children = vd.listChildren(path);
            for (Path child : children) {
                System.out.println(child.toString());
            }
        } catch (IOException x) {
            x.printStackTrace();
        }
    }

    /**
     * Test searching
     */
    @Test
    public void testSearching() {
        String path = "/dir";

        importFile(DIR, path);
        try {
            SearchFilter filter = new SearchFilter();
            String[] keywords = {"java"};
            List<Path> resultList = vd.search("/", filter, keywords);
            for (Path result : resultList) {
                System.out.println(result.toString());
            }
        } catch (Exception x) {
            x.printStackTrace();
        }
    }
}