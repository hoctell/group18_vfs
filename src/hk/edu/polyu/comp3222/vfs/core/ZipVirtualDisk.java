package hk.edu.polyu.comp3222.vfs.core;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A virtual disk.
 */
public class ZipVirtualDisk implements VirtualDisk {
    private static final String CONFIG_PATH = "/.config.txt";

    private Path file;
    private URI fileUri;
    private FileSystem zipFs;
    private List<String> config = new ArrayList<>();
    private long maxSize;

    /**
     * Default constructor.
     * @param file the path of the physical file located
     * @throws IOException Exception if IO eror occur
     */
    public ZipVirtualDisk(Path file) throws IOException {
        this.file = file;
        if (isCreated()) {
            String absolutePath = file.toAbsolutePath().toString();
            if (System.getProperty("file.separator").equals("\\")) {
                absolutePath = absolutePath.replace("\\", "/");
            }
            if (!absolutePath.startsWith ("/")) {
                absolutePath = "/" + absolutePath;
            }
            fileUri = URI.create("jar:file:"+absolutePath);
            openFileSystem();

            readConfig();
        }
    }

    private void openFileSystem()
            throws IOException {
        if (zipFs != null && zipFs.isOpen()) return;
        Map<String, String> env = new HashMap<>();
        env.put("create", "false");
        try {
            zipFs = FileSystems.getFileSystem(fileUri);
        } catch (FileSystemNotFoundException x){
            zipFs = FileSystems.newFileSystem(fileUri, env, null);
        }
    }

    private void readConfig()
            throws IOException {
        openFileSystem();
        config = Files.readAllLines(getPath(CONFIG_PATH));
        maxSize = Long.parseLong(config.get(0));
    }

    private void writeConfig()
            throws IOException {
        openFileSystem();
        config.clear();
        config.add(String.valueOf(maxSize));
        Files.write(getPath(CONFIG_PATH), config, Charset.defaultCharset());
        close();
        openFileSystem();
    }

    private void checkPhysicalExistence()
            throws IOException {
        if (!isCreated())
            throw new FileNotFoundException(file + "is not found. It may be not created or be deleted");
    }

    @Override
    public Path getPath(final String path) {
        if (path.equals(""))
            return zipFs.getPath("/");
        return zipFs.getPath(path);
    }

    @Override
    public String getBaseName() {
        String[] parts = file.getFileName().toString().split("\\.");
        return parts[0];
    }

    @Override
    public long getMaxSize() {
        return maxSize;
    }

    @Override
    public long getFreeSpace()
            throws IOException {
        return getMaxSize() - getOccupiedSpace();
    }

    @Override
    public long getOccupiedSpace()
            throws IOException {
        return Files.size(file);
    }

    @Override
    public boolean hasEnoughSpace(final long spaceSize)
            throws IOException {
        return getOccupiedSpace() + spaceSize <= getMaxSize();
    }

    /**
     * @return whether the file is exits
     */
    public boolean isCreated() {
        return Files.exists(file);
    }

    @Override
    public boolean exist(final String path) {
        if (path.equals(""))
            return false;
        Path zipFsPath = zipFs.getPath(path);
        return Files.exists(zipFsPath);
    }

    @Override
    public boolean notExist(final String path) {
        if (path.equals(""))
            return true;
        Path zipFsPath = zipFs.getPath(path);
        return Files.notExists(zipFsPath);
    }

    /**
     * Delete file from the virtual disk to the host file system
     * @param path string represents source path in the virtual disk
     * @return  whether the path is a directory
     */
    @Override
    public boolean isDirectory(final String path) {
        Path zipFsPath = getPath(path);
        return Files.isDirectory(zipFsPath);
    }

    @Override
    public void close()
            throws IOException {
        zipFs.close();
    }

    /**
     * Create physical file which stores the virtual disk
     * @param maxSize the max size allowed of virtual disk
     * @throws IOException if the physical file cannot be created
     */
    @Override
    public void createPhysicalFile(final long maxSize)
            throws IOException {
        if (isCreated())
            throw new FileAlreadyExistsException(file.toString());

        String absolutePath = file.toAbsolutePath().toString();
        if (System.getProperty("file.separator").equals("\\")) {
            absolutePath = absolutePath.replace("\\", "/");
        }
        if (!absolutePath.startsWith ("/")) {
            absolutePath = "/" + absolutePath;
        }

        Map<String, String> env = new HashMap<>();
        env.put("create", String.valueOf(Files.notExists(file)));
        fileUri = URI.create("jar:file:"+absolutePath);
        zipFs = FileSystems.newFileSystem(fileUri, env, null);

        this.maxSize = maxSize;
        writeConfig();
    }

    /**
     * Dispose the existing virtual disk
     * @throws IOException if the physical file cannot be deleted
     */
    @Override
    public void dispose()
            throws IOException {
        close();
        Files.delete(file);
    }

    /**
     * Import file form the host file system to the virtual disk
     * @param importPath import Path of source file
     * @param dest string represents destination path in the virtual disk
     * @throws IOException if importing file cannot be done
     */
    @Override
    public void importFiles(final Path importPath, final String dest)
            throws IOException, NotEnoughSpaceExpection {
        checkPhysicalExistence();
        if (Files.notExists(importPath))
            throw new FileNotFoundException();
        if (!hasEnoughSpace(Files.size(importPath)))
            throw new NotEnoughSpaceExpection(getBaseName());

        openFileSystem();
        Path zipFsPath = getPath(dest);
        if (Files.isRegularFile(importPath)) {
            if (zipFsPath.getParent() != null) {
                Files.createDirectories(zipFsPath.getParent());
            }
            Files.copy(importPath, zipFsPath);
        } else if (Files.isDirectory(importPath)){
            Files.walkFileTree(importPath, new CopyFileVisitor(importPath, zipFsPath));
        }
        close();
        openFileSystem();

        assert getOccupiedSpace() <= getMaxSize();
    }

    /**
     * Export file from the virtual disk to the host file system
     * @param src string represents source path in the virtual disk
     * @param exportPath export Path of destination file
     * @throws IOException if exporting file cannot be done
     */
    @Override
    public void exportFiles(final String src, final Path exportPath)
            throws IOException {
        checkPhysicalExistence();
        if (notExist(src))
            throw new FileNotFoundException();

        openFileSystem();
        Path zipFsPath = getPath(src);
        if (Files.isRegularFile(zipFsPath)) {
            if (exportPath.getParent() != null) {
                Files.createDirectories(exportPath.getParent());
            }
            Files.copy(zipFsPath, exportPath);
        } else if (Files.isDirectory(zipFsPath)) {
            Files.walkFileTree(zipFsPath, new CopyFileVisitor(zipFsPath, exportPath));
        }
    }

    /**
     * Delete file from the virtual disk to the host file system
     * @param target string represents source path in the virtual disk
     * @throws IOException if exporting file cannot be done
     */
    @Override
    public void delete(final String target)
            throws IOException {
        checkPhysicalExistence();
        if (notExist(target))
            throw new FileNotFoundException();
        if (getPath(CONFIG_PATH).equals(getPath(target)))
            throw new IOException("Not allowed deletion");

        openFileSystem();
        Path zipFsPath = getPath(target);
        Files.walkFileTree(zipFsPath, new DeleteFileVisitor());
        close();
        openFileSystem();
    }

    /**
     * Delete file from the virtual disk to the host file system
     * @param src string represents source path in the virtual disk
     * @param target string represets target path in the virtual disk
     * @throws IOException if exporting file cannot be done
     */

    @Override
    public void copy(final String src, final String target)
            throws IOException, NotEnoughSpaceExpection {
        checkPhysicalExistence();
        if (notExist(src))
            throw new FileNotFoundException();

        openFileSystem();
        Path fromPath = (src.startsWith("/")) ? getPath(src) : getPath("/"+src);
        if (!hasEnoughSpace(Files.size(fromPath)))
            throw new NotEnoughSpaceExpection(getBaseName());
        Path toPath = (target.startsWith("/")) ? getPath(target) : getPath("/"+target);

        if (Files.isRegularFile(fromPath)) {
            if (toPath.getParent() != null) {
                Files.createDirectories(toPath.getParent());
            }
            Files.copy(fromPath, toPath);
        } else if (Files.isDirectory(fromPath)){
            Files.walkFileTree(fromPath, new CopyFileVisitor(fromPath, toPath));
        }
        close();
        openFileSystem();

        assert getOccupiedSpace() <= getMaxSize();
    }

    /**
     * Delete file from the virtual disk to the host file system
     * @param src string represents source path in the virtual disk
     * @param  target string represets target path in the virtual disk
     * @throws IOException if exporting file cannot be done
     */
    @Override
    public void move(final String src, final String target)
            throws IOException, NotEnoughSpaceExpection {
        checkPhysicalExistence();
        if (notExist(src))
            throw new FileNotFoundException();

        openFileSystem();
        copy(src, target);
        delete(src);
        close();
        openFileSystem();
    }

    /**
     * Delete file from the virtual disk to the host file system
     * @param target string represents source path in the virtual disk
     * @param  newName string represets the newName of the Path in the virtual disk
     * @throws IOException if exporting file cannot be done
     */
    @Override
    public void rename(final String target, final String newName)
            throws IOException, NotEnoughSpaceExpection {
        checkPhysicalExistence();
        if (notExist(target))
            throw new FileNotFoundException();

        openFileSystem();
        Path targetPath = getPath(target);
        Path newPath = targetPath.resolveSibling(newName);
        move(target, newPath.toString());
        close();
        openFileSystem();
    }

    /**
     * Delete file from the virtual disk to the host file system
     * @param target string represents source path in the virtual disk
     * @return  the chiildren list  in the path
     * @throws IOException if exporting file cannot be done
     */
    @Override
    public List<Path> listChildren(final String target)
            throws IOException {
        checkPhysicalExistence();
        Path zipFsPath = getPath(target);
        ChildrenFileVisitor visitor = new ChildrenFileVisitor(zipFsPath);
        Files.walkFileTree(zipFsPath, visitor);
        return visitor.getChildren();
    }

    @Override
    public void createFile(final String path)
            throws IOException {
        checkPhysicalExistence();
        openFileSystem();
        Path zipFsPath = getPath(path);
        Files.createFile(zipFsPath);
        close();
        openFileSystem();
    }

    @Override
    public void createDirectory(final String path)
            throws IOException {
        checkPhysicalExistence();
        openFileSystem();
        Path zipFsPath = getPath(path);
        Files.createDirectory(zipFsPath);
        close();
        openFileSystem();
    }

    @Override
    public List<Path> search(final String start, final SearchFilter filter, final String[] keywords)
            throws IOException {
        SearchFileVisitor visitor = new SearchFileVisitor(filter, keywords);
        Files.walkFileTree(getPath(start), visitor);
        return visitor.getSearchResult();
    }

    @Override
    public void print()
            throws IOException {
        System.out.println("[VD:"+getBaseName()+"]");
        Files.walkFileTree(getPath("/"), new PrintFileVisitor());
    }

    @Override
    public String toString() {
        return getBaseName();
    }
}
