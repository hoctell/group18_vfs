package hk.edu.polyu.comp3222.vfs.client;

import hk.edu.polyu.comp3222.vfs.core.SearchFilter;
import hk.edu.polyu.comp3222.vfs.core.VFS;
import hk.edu.polyu.comp3222.vfs.core.VirtualDisk;

import java.io.*;
import java.net.Socket;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Lok on 29/3/2017.
 */

/**
 * The command interface
 */
interface Command{
    /**
     * Execute the action
     */
    void doAction();
}

/**
 * The receiver class
 */
class Action{
    /**
     * Create a new virtual disk
     * @param msg input the directory for the action
     *@param vfs the client Virtual file system
     */
    public void createVd(String msg,VFS vfs) {
        try {
            String[] arr = msg.split(" ");
            if(arr.length == 4) {
                vfs.createVirtualDisk(Paths.get(arr[1], arr[2]), Integer.parseInt(arr[3]));
                File file = new File("resources/record.txt");
                FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
                BufferedWriter bw = new BufferedWriter(fw);
                bw.write(arr[2]);
                bw.newLine();
                bw.close();
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }
    /**
     * Diposal the vitrual disk
     * @param msg input the directory for the action
     * @param vfs the client Virtual file system
     */
    public void dispoalVd(String msg,VFS vfs){
        try {
            String[] arr = msg.split(" ");
            if(arr.length == 2) {
                VirtualDisk vd = vfs.getVirtualDisk(arr[1]);
                vfs.deleteVirtualDisk(vd);
                Scanner s = new Scanner(new File("resources/record.txt"));
                List<String> list = new ArrayList<String>();
                while (s.hasNextLine()) {
                    list.add(s.nextLine());
                }
                s.close();
                list.remove(arr[1] + ".vd");
                File file = new File("resources/record.txt");
                FileWriter fw = new FileWriter(file.getAbsoluteFile(), false);
                BufferedWriter bw = new BufferedWriter(fw);
                for (String str : list) {
                    bw.write(str);
                    bw.newLine();
                }
                bw.close();
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }
    /**
     * Create/ Delete/ Rename dirtory or file
     * @param msg input the directory for the action
     * @param vfs the client Virtual file system
     */
    public void controlVd(String msg,VFS vfs){
        try {
            String[] arr = msg.split(" ");
            if (arr.length == 3 && arr[1].equals("create")) {
                vfs.createFile(arr[2]);
            } else if (arr.length == 3 && arr[1].equals("delete")) {
                vfs.delete(arr[2]);
            } else if (arr.length == 4 && arr[1].equals("rename")) {
                vfs.rename(arr[2],arr[3]);
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }
    /**
     * Move/ Copy dirtory or file
     * @param msg input the directory for the action
     *@param vfs the client Virtual file system
     */
    public void moveOrCopy(String msg,VFS vfs) {
        try {
            String[] arr = msg.split(" ");
            if(arr.length == 4 && arr[1].equals("move"))
                vfs.move(arr[2], arr[3]);
            else if(arr.length == 4 && arr[1].equals("copy")){
                vfs.copy(arr[2],arr[3]);
            }
        }catch (Exception e) {
            System.out.println(e.toString());
        }
    }
    /**
     * Import directory or file
     * @param msg input the directory for the action
     * @param vfs the client Virtual file system
     */
    public void importAction(String msg,VFS vfs) {
        try {
            String[] arr = msg.split(" ");
            if(arr.length == 3) {
                vfs.importFiles(Paths.get(arr[1]), arr[2]);
            }
        }catch (Exception e) {
            System.out.println(e.toString());
        }
    }
    /**
     * Export directory or file
     * @param msg input the directory for the action
     * @param vfs the client Virtual file system
     */
    public void exportAction(String msg,VFS vfs) {
        try{
            String[] arr = msg.split(" ");
            if(arr.length == 3) {
                vfs.exportFiles(arr[1], Paths.get(arr[2]));
            }
        }catch (Exception e) {
            System.out.println(e.toString());
        }
    }
    /**
     * Display the size of the virtual disk
     * @param msg input the directory for the action
     *@param vfs the client Virtual file system
     */
    public void displaySize(String msg,VFS vfs) {
        try {
            String[] arr = msg.split(" ");
            if(arr.length == 2) {
                System.out.println("Occupied Space: " + vfs.getVirtualDisk(arr[1]).getOccupiedSpace());
                System.out.println("Free Space: " + vfs.getVirtualDisk(arr[1]).getFreeSpace());
            }
        }catch (Exception e) {
            System.out.println(e.toString());
        }
    }
    /**
     * List all the files and folders in the disk or Change to other directory
     * @param msg input the virtualDisk
     *@param vfs the client Virtual file system
     */
    public void listOrChangeDir(String msg,VFS vfs){
        try {
            String[] arr = msg.split(" ");
            if(arr.length == 2 && arr[1].equals("list")) {
                List<String> strList = vfs.list();
                for (String str : strList) {
                    System.out.println(str);
                }
            }
            else if(arr.length == 3 && arr[1].equals("change")){
                vfs.changeDir(arr[2]);
                System.out.println(vfs.getWorkingDir());
            }
        }catch (Exception e) {
            System.out.println(e.toString());
        }
    }

    /**
     * search the keyword
     * @param msg client's input
     * @param vfs client's vfs
     */
    public void search(String msg, VFS vfs){
        //arr[1] = startPath, arr[2] = file/ dir/ both, arr[3] = yes/no, arr[4] = all/any, arr[5-n] = keyword
        try {
            String[] arr = msg.split(" ");
            if(arr.length >= 6) {
                SearchFilter filter = new SearchFilter();
                if (arr[2].equals("file"))
                    filter.setFilter(SearchFilter.MATCH_FILE);
                else if (arr[2].equals("dir"))
                    filter.setFilter(SearchFilter.MATCH_DIR);
                else if (arr[2].equals("both"))
                    filter.setFilter(SearchFilter.MATCH_BOTH_TYPE);
                if (arr[3].equals("yes"))
                    filter.setFilter(SearchFilter.SENSITIVE);
                else if (arr[3].equals("no"))
                    filter.setFilter(SearchFilter.INSENSITIVE);
                if (arr[4].equals("all"))
                    filter.setFilter(SearchFilter.MATCH_ALL);
                else if (arr[4].equals("any"))
                    filter.setFilter(SearchFilter.MATCH_ANY);
                String[] keyWord = Arrays.copyOfRange(arr, 5, arr.length);
                List<String> target = vfs.search(arr[1], filter, keyWord);
                for (String path : target)
                    System.out.println(path);
            }
        }catch (Exception e) {
            System.out.println(e.toString());
        }
    }
}

/**
 * The invoker class
 */
class Execute{
    /**
     * execute the command
     * @param cmd the client choice
     */
    public void execute(Command cmd){
        cmd.doAction();
    }
}

/**
 * Create a new virtual disk
 */
class CreateVdCommand implements Command{
    private String msg;
    private VFS vfs;
    private Action action = new Action();
    /**
     * Constructor of this class
     * @param msg Client's command
     * @param vfs Client's virtual file system
     */
    public CreateVdCommand(String msg, VFS vfs){
        this.msg = msg;
        this.vfs = vfs;
    }
    @Override
    public void doAction(){
        action.createVd(msg,vfs);
    }
}
/**
 * Dispoal a virtual disk
 */
class DispoalVdCommand implements Command{
    private String msg;
    private VFS vfs;
    private Action action = new Action();
    /**
     * Constructor of this class
     * @param msg Client's command
     * @param vfs Client's virtual file system
     */
    public DispoalVdCommand(String msg, VFS vfs){
        this.msg = msg;
        this.vfs = vfs;
    }
    @Override
    public void doAction(){
        action.dispoalVd(msg,vfs);
    }
}
/**
 * Control a virtual disk
 */
class ControlVdCommand implements Command{
    private String msg;
    private VFS vfs;
    private Action action = new Action();
    /**
     * Constructor of this class
     * @param msg Client's command
     * @param vfs Client's virtual file system
     */
    public ControlVdCommand(String msg, VFS vfs){
        this.msg = msg;
        this.vfs = vfs;
    }
    @Override
    public void doAction(){
        action.controlVd(msg,vfs);
    }
}
/**
 * List the thing in VD
 */
class ListVdCommand implements Command{
    private String msg;
    private VFS vfs;
    private Action action = new Action();
    /**
     * Constructor of this class
     * @param msg Client's command
     * @param vfs Client's virtual file system
     */
    public ListVdCommand(String msg, VFS vfs){
        this.msg = msg;
        this.vfs = vfs;
    }
    @Override
    public void doAction(){
        action.listOrChangeDir(msg,vfs);
    }
}
/**
 * Move or copy file from a virtual disk
 */
class MoveOrCopyVdCommand implements Command{
    private String msg;
    private VFS vfs;
    private Action action = new Action();
    /**
     * Constructor of this class
     * @param msg Client's command
     * @param vfs Client's virtual file system
     */
    public MoveOrCopyVdCommand(String msg, VFS vfs){
        this.msg = msg;
        this.vfs = vfs;
    }
    @Override
    public void doAction(){
        action.moveOrCopy(msg,vfs);
    }
}
/**
 * import a file to virtual disk
 */
class ImportVdCommand implements Command{
    private String msg;
    private VFS vfs;
    private Action action = new Action();
    /**
     * Constructor of this class
     * @param msg Client's command
     * @param vfs Client's virtual file system
     */
    public ImportVdCommand(String msg, VFS vfs){
        this.msg = msg;
        this.vfs = vfs;
    }
    @Override
    public void doAction(){
        action.importAction(msg,vfs);
    }
}
/**
 * Export a file from virtual disk
 */
class ExportVdCommand implements Command{
    private String msg;
    private VFS vfs;
    private Action action = new Action();
    /**
     * Constructor of this class
     * @param msg Client's command
     * @param vfs Client's virtual file system
     */
    public ExportVdCommand(String msg, VFS vfs){
        this.msg = msg;
        this.vfs = vfs;
    }
    @Override
    public void doAction(){
        action.exportAction(msg,vfs);
    }
}
/**
 * Display the size of a virtual disk
 */
class DisplayVdCommand implements Command{
    private String msg;
    private VFS vfs;
    private Action action = new Action();
    /**
     * Constructor of this class
     * @param msg Client's command
     * @param vfs Client's virtual file system
     */
    public DisplayVdCommand(String msg, VFS vfs){
        this.msg = msg;
        this.vfs = vfs;
    }
    @Override
    public void doAction(){
        action.displaySize(msg,vfs);
    }
}

/**
 * Show Default
 */
class DefaultVdCommand implements Command{
    @Override
    public void doAction(){
        System.out.println("");
    }
}

/**
 * Search keyword
 */
class SearchVdCommand implements Command{
    private String msg;
    private VFS vfs;
    private Action action = new Action();
    /**
     * Constructor of this class
     * @param msg Client's command
     * @param vfs Client's virtual file system
     */
    public SearchVdCommand(String msg, VFS vfs){
        this.msg = msg;
        this.vfs = vfs;
    }
    @Override
    public void doAction(){
        action.search(msg,vfs);
    }
}

/**
 * Client Class
 */
public class Client {
    private String address = "127.0.0.1";
    private final int port = 8888;
    private VFS vfs;
    private Socket socket;
    private String state = "Offline";
    private DataOutputStream out;
    private Boolean exit = false;
    /**
     * Constructor of Client
     */
    public Client(){
        try {
            List<Path> files = new ArrayList<>();
            FileReader fr = new FileReader("resources/record.txt");
            BufferedReader br = new BufferedReader(fr);
            String cur;
            while((cur = br.readLine()) != null){
                files.add(Paths.get("resources/"+cur));
            }
            vfs = new VFS(files);
        } catch(IOException e){
            System.out.println(e.toString());
        }
    }
    /**
     * Close the VFS for safety
     */
    public void close(){
        try {
            vfs.close();
        }catch(IOException e){
            System.out.println(e.toString());
        }
    }

    /**
     * Ask Client action
     * @param msg Client's choice
     */
    public void askForAction(String msg){
        System.out.println("State: "+state);
        System.out.println("Please enter your action:");
        System.out.println("1. Create Virtual Disk ");
        System.out.println("\tFormat: 1 [external pathname]/[Virtual Disk name] [size]\te.g. 1 resources/ client.vd 10000000");
        System.out.println("2. Dispose Virtual Disk ");
        System.out.println("\tFormat: 2 [Virtual Disk name]\t\t e.g. 2 client");
        System.out.println("3. Create/ Delete/ Rename dirtory or file ");
        System.out.println("\tFormat: 3 create [pathname]/[file name/Directory name]\t e.g. 3 create /client/temp/");
        System.out.println("\tFormat: 3 rename [pathname]/[file name/Directory name] [new name]\t e.g. 3 rename /client/temp temp2");
        System.out.println("\tFormat: 3 delete [pathname]/[file name/Directory name]\t e.g. 3 delete /client/temp2");


        System.out.println("4. List/ Change Dirtory ");
        System.out.println("\tFormat: 4 list");
        System.out.println("\tFormat: 4 change [pathname]\t e.g. 4 change /client/temp");
        System.out.println("5. Move/ Copy dirtory or file ");
        System.out.println("\tFormat: 5 move [source] [destination]\t e.g. 5 move /client/temp /client/temp2/temp");
        System.out.println("\tFormat: 5 copy [source] [destination]\t e.g. 5 copy /client/temp.txt /client/temp/temp.txt");

        System.out.println("6. Import directory or file ");
        System.out.println("\tFormat: 6 [external pathname] [destination]\t e.g. 6 resources/project_description.pdf /client/project_description.pdf");

        System.out.println("7. Export directory or file ");
        System.out.println("\tFormat: 7 [source] [external pathname]\t e.g. 7 /client/project_description.pdf resources/temp.pdf");

        System.out.println("8. Display the size of Virtual disk ");
        System.out.println("\tFormat: 8 [Vistual Disk Name]\t e.g. 8 client");

        System.out.println("9. Search ");
        System.out.println("\tFormat: 9 [pathname] file [sensitive] [any/all] [keywords]\t e.g. 9 /client/VFS-template file yes all vfs.iml");
        System.out.println("\tFormat: 9 [pathname] dir [sensitive] [any/all] [keywords]\t e.g. 9 /client/VFS-template dir yes all src");
        System.out.println("\tFormat: 9 [pathname] both [non-sensitive] [any/all] [keywords]\t e.g. 9 /client/VFS-template both yes all src");
        System.out.println("\tFormat: 9 [pathname] both [non-sensitive] [any/all] [keywords]\t e.g. 9 /client/VFS-template file no all vfs.iml");
        System.out.println("\tFormat: 9 [pathname] both [non-sensitive] [any/all] [keywords]\t e.g. 9 /client/VFS-template file no any vfs.iml\n");



        System.out.println("C. connect to server");
        System.out.println("\tFormat: C IPaddress");

        System.out.println("L. link virtual disk to server");
        System.out.println("\tFormat: [external pathname] [virtual disk name]\t e.g. L resources/client.vd client.vd");

        System.out.println("D. Disconnect");
        System.out.println("\tFormat: D");
        System.out.println("E. Exit");
        System.out.println("\tFormat: E");
        if(msg.equals("")){
            Scanner input = new Scanner(System.in);
            msg = input.nextLine();
        }
        String[] arr = msg.split(" ");
        Command cmd = new DefaultVdCommand();
        switch(arr[0].charAt(0)){
            case '1':
                cmd = new CreateVdCommand(msg,vfs);
                break;
            case '2':
                cmd = new DispoalVdCommand(msg,vfs);
                break;
            case '3':
                cmd = new ControlVdCommand(msg,vfs);
                break;
            case '4':
                cmd = new ListVdCommand(msg,vfs);
                break;
            case '5':
                cmd = new MoveOrCopyVdCommand(msg,vfs);
                break;
            case '6':
                cmd = new ImportVdCommand(msg,vfs);
                break;
            case '7':
                cmd = new ExportVdCommand(msg,vfs);
                break;
            case '8':
                cmd = new DisplayVdCommand(msg,vfs);
                break;
            case '9':
                cmd = new SearchVdCommand(msg,vfs);
                break;
            case 'C':
                if(arr.length == 2) {
                    connect(arr[1]);
                }
                else if(arr.length == 4) {
                    connect(arr[1] + " " + arr[2] + " " + arr[3]);
                }
                break;
            case 'L':
                if(arr.length == 3) {
                    linkVd(msg);
                }
                break;
            case 'D':
                if(arr.length == 1) {
                    disconnect();
                }
                break;
            case 'E':
                exit = true;
                break;
            case 'P':
                try {
                    vfs.print();
                }catch(IOException e){
                    System.out.println(e.toString());
                }
                break;
            default:
                System.out.println("Input Error!");
                break;
        }
        Execute execute = new Execute();
        execute.execute(cmd);
    }

    /**
     * connect to server
     * @param ans Client's input
     */
    public void connect(String ans){
        try{
            String[] testLength = ans.split(" ");
            if(testLength.length == 1){
                address = ans;
            }
            socket = new Socket(address,port);
            DataInputStream in = new DataInputStream(socket.getInputStream());
            out = new DataOutputStream(socket.getOutputStream());
            BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
            String msg ="";
            String rpy ="";
            while(true){
                rpy = in.readUTF();
                System.out.println(rpy);
                if(!rpy.equals("Login success!")) {
                    if(testLength.length == 1)
                        msg = read.readLine();
                    else {
                        msg = ans;
                    }
                    out.writeUTF(msg);
                    out.flush();
                }
                else
                    break;
            }
            state = "Online";
        }catch(java.io.IOException e) {
            System.out.println(e.toString());
        }
    }

    /**
     * link the Vd to server
     * @param msg Client answer
     */
    public void linkVd(String msg){
        try {
            String[] arr = msg.split(" ");
            System.out.println("FILE: " + arr[1]);
            File f =new  File(arr[1]);
            FileInputStream fin = new FileInputStream(f);
            String s = f.getName();
            long fs = f.length();
            final int MaxSize = 4096;
            DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
            dos.writeUTF(s);
            dos.writeLong(fs);
            DataInputStream in = new DataInputStream(socket.getInputStream());
            String rpy = in.readUTF();
            System.out.println(rpy);
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(f)));
            byte[] readData = new byte[MaxSize];
            int i;
            while((i = fin.read(readData)) != -1)
            {
                dos.write(readData, 0, i);
            }
            fin.close();
            br.close();
            rpy = in.readUTF();
            System.out.println(rpy);
        }catch(IOException e){
            System.out.println(e.toString());
        }
    }

    /**
     * disconnect from the server
     */
    public void disconnect(){
        try {
            out.writeUTF("Disconnect");
            out.flush();
            socket.close();
            state = "Offline";
        } catch(IOException e){
            System.out.println(e.toString());
        }
    }

    /**
     * Check whether client want to exit the program
     * @return The value of exit
     */
    public boolean isExit(){
        return exit;
    }
    /**
     * Main function
     * @param args the argument array
     */
    public static void main(String[] args){
        Client client = new Client();
        while(!client.isExit()){
            client.askForAction("");
        }
        client.close();
    }
}
