package hk.edu.polyu.comp3222.vfs.core;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

import static java.nio.file.FileVisitResult.CONTINUE;

/**
 * Created by hoctell on 28/2/2017.
 */
public class PrintFileVisitor extends SimpleFileVisitor<Path> {

    // Print information about
    // each type of file.
    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attr) {
        if (file.getFileName().toString().startsWith(".")) {
            return CONTINUE;
        } else if (attr.isRegularFile()) {
            System.out.format("Regular file: %s ", file);
        }
        System.out.println("(" + attr.size() + "bytes)");
        return CONTINUE;
    }

    // Print each directory visited.
    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
        System.out.format("Directory: %s%n", dir);
        return CONTINUE;
    }
}