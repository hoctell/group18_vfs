package hk.edu.polyu.comp3222.vfs.core;

import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hoctell on 3/4/2017.
 */
public class SearchFileVisitor extends SimpleFileVisitor<Path> {
    private List<Path> searchResult = new ArrayList<>();
    private SearchFilter filter;
    private String[] keywords;

    /**
     * constructor
     *@param filter represent the filter use
     *@param  keywords represent the keywords used
     */
    public SearchFileVisitor(SearchFilter filter, String[] keywords) {
        this.filter = filter;
        this.keywords = keywords;
    }

    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
        if (filter.isMatch(dir, keywords))
            searchResult.add(dir);
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
        if (filter.isMatch(file, keywords))
            searchResult.add(file);
        return FileVisitResult.CONTINUE;
    }

    /**
     *@return the search reult list
     */
    public List<Path> getSearchResult() {
        return searchResult;
    }
}
