package hk.edu.polyu.comp3222.vfs.test;

/**
 * Created by Lok on 30/3/2017.
 */
import hk.edu.polyu.comp3222.vfs.client.Client;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
/**
 * Client Test
 */
public class ClientTest {
    private Client client = new Client();
    /**
     * Create 2 test virtual disk
     */
    @org.junit.Before
    public void setUp(){
        client.askForAction("1 resources/ client.vd 10000000");
        client.askForAction("1 resources/ client2.vd 10000000");
    }
    /**
     * dispoal the two virtual disk
     */
    @org.junit.After
    public void tearDown(){
        client.askForAction("2 client");
        client.askForAction("2 client2");
    }
    /**
     * Test create, rename and delete directory or file
     */
    @Test
    public void testAction3(){
        client.askForAction("3 create /client/temp/");
        client.askForAction("3 create /client2/temp.txt");
        System.out.println("-------------------------------------------------------------------");
        client.askForAction("P");
        client.askForAction("3 rename /client/temp temp2");
        client.askForAction("3 rename /client2/temp.txt temp2.txt");
        System.out.println("-------------------------------------------------------------------");
        client.askForAction("P");
        client.askForAction("3 delete /client/temp2");
        client.askForAction("3 delete /client2/temp2.txt");
        System.out.println("-------------------------------------------------------------------");
        client.askForAction("P");
    }
    /**
     * Test List and Change directory function
     */
    @Test
    public void testAction4(){
        client.askForAction("3 create /client/temp/");
        client.askForAction("3 create /client/temp/1.txt");
        client.askForAction("3 create /client/temp/2.txt");
        client.askForAction("3 create /client/temp/3.txt");
        client.askForAction("4 list");
        client.askForAction("4 change /client/temp");
        client.askForAction("4 list");
        System.out.println("-------------------------------------------------------------------");
        client.askForAction("P");
    }
    /**
     * Test Move and Copy function
     */
    @Test
    public void testAction5(){
        client.askForAction("3 create /client/temp/");
        client.askForAction("3 create /client/temp2/");
        client.askForAction("3 create /client/temp.txt");
        System.out.println("-------------------------------------------------------------------");
        client.askForAction("P");
        client.askForAction("5 copy /client/temp.txt /client/temp/temp.txt");
        System.out.println("-------------------------------------------------------------------");
        client.askForAction("P");
        client.askForAction("5 move /client/temp /client/temp2/temp");
        System.out.println("-------------------------------------------------------------------");
        client.askForAction("P");
    }
    /**
     * Test Import function
     */
    @Test
    public void testAction6(){
        client.askForAction("6 resources/project_description.pdf /client/project_description.pdf");
        System.out.println("-------------------------------------------------------------------");
        client.askForAction("P");
    }
    /**
     * Test Export function
     */
    @Test
    public void testAction7(){
        client.askForAction("6 resources/project_description.pdf /client/project_description.pdf");
        System.out.println("-------------------------------------------------------------------");
        client.askForAction("P");
        client.askForAction(("7 /client/project_description.pdf resources/temp.pdf"));
        try {
            Files.delete(Paths.get("resources/temp.pdf"));
        }catch(IOException e){
            e.printStackTrace();
        }
        System.out.println("-------------------------------------------------------------------");
        client.askForAction("P");
    }
    /**
     * Test display size function
     */
    @Test
    public void testAction8(){
        client.askForAction(("8 client"));
        client.askForAction("6 resources/project_description.pdf /client/project_description.pdf");
        client.askForAction(("8 client"));
    }

    /**
     * Test search function with different filter
     */
    @Test
    public void testAction9(){
        client.askForAction("6 resources/VFS-template /client/VFS-template");
        System.out.println("-----------------------");
        client.askForAction("9 /client/VFS-template file yes all vfs.iml");
        client.askForAction("9 /client/VFS-template dir yes all src");
        client.askForAction("9 /client/VFS-template both yes all src");
        client.askForAction("9 /client/VFS-template file no all vfs.iml");
        client.askForAction("9 /client/VFS-template file no any vfs iml");
    }
}
