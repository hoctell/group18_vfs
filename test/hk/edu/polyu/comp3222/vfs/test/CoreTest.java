package hk.edu.polyu.comp3222.vfs.test;

import hk.edu.polyu.comp3222.vfs.core.SearchFilter;
import hk.edu.polyu.comp3222.vfs.core.VFS;
import hk.edu.polyu.comp3222.vfs.core.VirtualDisk;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * Core Test
 */
public class CoreTest {
    private static final String WORKING_DIR = "resources/";
    private static final String FILE_A = WORKING_DIR + "project_description.pdf";
    private static final String FILE_B = WORKING_DIR + "COMP3222_PROJECT.xml";
    private static final String DIR = WORKING_DIR + "VFS-template";
    private static final long MAX_SIZE = 10000000;

    private VFS vfs;
    private VirtualDisk first;
    private VirtualDisk second;

    /**
     * Create a VFS with two virtual disks
     * @throws Exception any exception
     */
    @org.junit.Before
    public void setUp() throws Exception {
        vfs = new VFS(new ArrayList<>());
        first = vfs.createVirtualDisk(Paths.get(WORKING_DIR, "first.vd"), MAX_SIZE);
        second = vfs.createVirtualDisk(Paths.get(WORKING_DIR, "second.vd"), MAX_SIZE);
    }

    /**
     * Print and clear all virtual disks
     */
    @org.junit.After
    public void tearDown() {
        for (Map.Entry<String, VirtualDisk> entry : vfs.getVirtualDiskMap().entrySet()) {
            try {
                VirtualDisk disk = entry.getValue();
                disk.print();
            } catch (IOException x) {
                x.printStackTrace();
            }
        }
        try {
            vfs.clearVirtualDisks();
        } catch (Exception x) {
            x.printStackTrace();
        }
    }

    /**
     * Test VFS handling path
     */
    @Test
    public void testHandlingPath() {
        assertEquals("/first/dirA", vfs.toAbsolutePath("first/dirA"));
        assertEquals("/second/dirA", vfs.toAbsolutePath("/second/dirA"));

        try {
            assertFalse(vfs.isValidPath("/first/dirA"));
            assertEquals("dirA", vfs.phraseDiskPath("first/dirA"));
            assertEquals("dirA/dirB", vfs.phraseDiskPath("/second/dirA/dirB"));
        } catch (Exception x) {
            System.out.println("VD not found");
        }
    }

    /**
     * Test VFS create virtual disk
     */
    @Test
    public void testVirtualDiskCreation() {
        try {
            vfs.createVirtualDisk(Paths.get(WORKING_DIR, "add.vd"), MAX_SIZE);
        } catch (Exception x) {
            x.printStackTrace();
        }
    }

    /**
     * Test VFS delete virtual disk
     */
    @Test
    public void testVirtualDiskDeletion() {
        try {
            vfs.createVirtualDisk(Paths.get(WORKING_DIR, "delete.vd"), MAX_SIZE);
            VirtualDisk disk = vfs.getVirtualDisk("delete");
            vfs.deleteVirtualDisk(disk);
        } catch (Exception x) {
            x.printStackTrace();
        }
    }

    /**
     * Test copying file across virtual disks
     */
    @Test
    public void testCopyingFileAcrossVDs() {
        String path = "in.pdf";
        String from = first + "/" + path;
        String to = second + "/" + path;

        try {
            vfs.importFiles(Paths.get(FILE_A), from);
            vfs.copy(from, to);
        } catch (Exception x) {
            x.printStackTrace();
        }
    }

    /**
     * Test moving file across virtual disks
     */
    @Test
    public void testMovingFileAcrossVDs() {
        String path = "in.pdf";
        String from = first + "/" + path;
        String to = second + "/" + path;

        try {
            vfs.importFiles(Paths.get(FILE_A), from);
            vfs.move(from, to);
        } catch (Exception x) {
            x.printStackTrace();
        }
    }

    /**
     * Test changing working directory in VFS
     */
    @Test
    public void testChangingDir() {
        try {
            vfs.importFiles(Paths.get(DIR), first + "/" + "dir");
            vfs.changeDir("first/dir/test");
            assertEquals("/first/dir/test/", vfs.getWorkingDir());

            vfs.changeDir("/first/dir");
            vfs.changeDir("/");
            assertEquals("/", vfs.getWorkingDir());

            vfs.changeDir("/first/dir");
            vfs.changeDir("/first");
            assertEquals("/first/", vfs.getWorkingDir());
        } catch (Exception x) {
            x.printStackTrace();
        }
    }

    /**
     * Test searching all virtual disks and search a virtual disk
     */
    @Test
    public void testSearching() {
        try {
            vfs.importFiles(Paths.get(DIR), "/first/dir");
            SearchFilter filter = new SearchFilter();
            String[] keywords = {"java"};
            List<String> resultList = vfs.search("/", filter, keywords);
            for (String result : resultList) {
                System.out.println(result);
            }

            resultList = vfs.search("/first", filter, keywords);
            for (String result : resultList) {
                System.out.println(result);
            }
        } catch (Exception x) {
            x.printStackTrace();
        }
    }

    /**
     * Test listing children of a virtual disk
     */
    @Test
    public void testListing() {
        try {
            vfs.importFiles(Paths.get(FILE_B), "/first/b.in");
            vfs.changeDir("/first");
            List<String> list = vfs.list();
            for (String result : list) {
                System.out.println(result);
            }
        } catch (Exception x) {
            x.printStackTrace();
        }
    }

    /**
     * Test opening VFS with existing virtual disks
     */
    @Test
    public void testOpeningVFS() {
        try {
            vfs.close();
            List<Path> files = new ArrayList<>();
            files.add(Paths.get(WORKING_DIR, "first.vd"));
            files.add(Paths.get(WORKING_DIR, "second.vd"));
            vfs = new VFS(files);
        } catch (Exception x) {
            x.printStackTrace();
        }
    }
}