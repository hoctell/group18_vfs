package hk.edu.polyu.comp3222.vfs.core;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;

/**
 * Created by hoctell on 5/4/2017.
 */
public class SearchFilter {
    /**
     *store the case sensitive
     */
    public static final CaseSensitive SENSITIVE = CaseSensitive.SENSITIVE;
    /**
     *store the case insensitive
     */
    public static final CaseSensitive INSENSITIVE = CaseSensitive.INSENSITiVE;
    /**
     *store the match keyword  of "any"
     */
    public static final MatchKeyword MATCH_ANY = MatchKeyword.ANY;
    /**
     *store the match keyword  of "all"
     */
    public static final MatchKeyword MATCH_ALL = MatchKeyword.ALL;
    /**
     *store the match file  of "file"
     */
    public static final MatchType MATCH_FILE = MatchType.FILE;
    /**
     *store the match file  of "dir"
     */
    public static final MatchType MATCH_DIR = MatchType.DIR;
    /**
     *store the match file  of "both"
     */
    public static final MatchType MATCH_BOTH_TYPE = MatchType.BOTH;

    private CaseSensitive sensitive;
    private MatchKeyword matchKeyword;
    private MatchType matchType;

    /**
     *constructor
     */
    public SearchFilter() {
        sensitive = CaseSensitive.SENSITIVE;
        matchKeyword = MatchKeyword.ANY;
        matchType = MatchType.FILE;
    }

    /**
     *@param sensitive represent the indicated sensitive
     */
    public void setFilter(CaseSensitive sensitive) {
        this.sensitive = sensitive;
    }

    /**
     *@param keyword represnet the indicated keyword
     */
    public void setFilter(MatchKeyword keyword) {
        matchKeyword = keyword;
    }

    /**
     *@param type represent the indicated type
     */
    public void setFilter(MatchType type) {
        matchType = type;
    }

    /**
     *@param target represent the target path
     *@param keywords represents the keyword used
     *@return  whhether the path match the keywords
     */
    boolean isMatch(Path target, String[] keywords) {
        if (target.getFileName() == null)
            return false;
        if (matchType == MatchType.FILE && Files.isDirectory(target))
            return false;
        if (matchType == MatchType.DIR && Files.isRegularFile(target))
            return false;
        String fileName = target.getFileName().toString();
        if (sensitive == CaseSensitive.INSENSITiVE)
            fileName = fileName.toUpperCase();
        if (matchKeyword == MatchKeyword.ANY)
            return Arrays.stream(keywords)
                    .map(s -> sensitive == CaseSensitive.SENSITIVE ? s : s.toUpperCase())
                    .anyMatch(fileName::contains);
        else
            return Arrays.stream(keywords)
                    .map(s -> sensitive == CaseSensitive.SENSITIVE ? s : s.toUpperCase())
                    .allMatch(fileName::contains);
    }
}
/**
 * enumber of case sensitive
 */
enum CaseSensitive {
    /**
     *one is sensitive
     */
    SENSITIVE,
    /**
     * one is insensitive
     */
    INSENSITiVE
}
/**
 * enumber of matching keyword
 */
enum MatchKeyword {
    /**
     *one is any
     */
    ANY,
    /**
     *one is all
     */ALL
}

/**
 * enumber of matching keyword
 */
enum MatchType {
    /**
     *one is file
     */
    FILE,
    /**
     *one is dir
     */DIR,
    /**
     *one is both
     */BOTH
}
