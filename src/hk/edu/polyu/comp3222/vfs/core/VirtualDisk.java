package hk.edu.polyu.comp3222.vfs.core;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

/**
 * Created by hoctell on 9/4/2017.
 */
public interface VirtualDisk {
    /**
     * @param  path  represent the stinrg of the path
     * @return the path
     */
    Path getPath(final String path);

    /**
     * @return a string of the the name of the file
     */
    String getBaseName();

    /**
     * @return  the max size of the virtual disk
     * @throws IOException Exception if IO eror occur
     */
    long getMaxSize()
            throws IOException;

    /**
     * @return  the free space f the virtual disk
     * @throws IOException Exception if IO eror occur
     */
    long getFreeSpace()
            throws IOException;

    /**
     * @return  the occupied space  of the virtual disk
     * @throws IOException Exception if IO eror occur
     */
    long getOccupiedSpace()
            throws IOException;

    /**
     * @param spaceSize represent the size of the disk
     * @return  where disk have enough space
     * @throws IOException Exception if IO eror occur
     */
    boolean hasEnoughSpace(long spaceSize)
            throws IOException;

    /**
     * @param path represent the file path
     * @return  whetehr the path is exist
     */
    boolean exist(String path);

    /**
     * @param path represent the file path
     * @return  whetehr the path is exist
     */
    boolean notExist(String path);

    /**
     * @param path represent the directory
     *  @return  whetehr the directory is ecist
     */
    boolean isDirectory(String path);

    /**
     * @throws IOException Exception if IO eror occur
     */
    void close()
            throws IOException;

    /**
     *  @param maxSize  represent the max size of the created file
     * @throws IOException Exception if IO eror occur
     */
    void createPhysicalFile(long maxSize)
            throws IOException;
    /**
     * @throws IOException Exception if IO eror occur
     */
    void dispose()
            throws IOException;

    /**
     * @param importPath  represent the file path
     * @param dest represent the destination path
     * @throws IOException Exception if IO eror occur
     * @throws NotEnoughSpaceExpection if importing files fails as not enough space
     */
    void importFiles(Path importPath, String dest)
            throws IOException, NotEnoughSpaceExpection;

    /**
     *  @param  src represent the scource path
     *  @param exportPath represnet the eexport path
     * @throws IOException Exception if IO eror occur
     */
    void exportFiles(String src, Path exportPath)
            throws IOException;

    /**
     * @param target represent the target path
     * @throws IOException Exception if IO eror occur
     */
    void delete(String target)
            throws IOException;

    /**
     * @param src represent the source path
     * @param target represent the target path
     * @throws IOException Exception if IO eror occur
     * @throws NotEnoughSpaceExpection if importing files fails as not enough space
     */
    void copy(String src, String target)
            throws IOException, NotEnoughSpaceExpection;

    /**
     * @param src represent the source path
     * @param target represent the target path
     * @throws IOException Exception if IO eror occur
     * @throws NotEnoughSpaceExpection if importing files fails as not enough space
     */
    void move(String src, String target)
            throws IOException, NotEnoughSpaceExpection;

    /**
     * @param target represent the target path
     * @param newName  represent the new name of the file
     * @throws IOException Exception if IO eror occur
     * @throws NotEnoughSpaceExpection if importing files fails as not enough space
     */
    void rename(String target, String newName)
            throws IOException, NotEnoughSpaceExpection;

    /**
     * @param target represent the target path
     * @return the list children of the target path
     * @throws IOException Exception if IO eror occur
     */
    List<Path> listChildren(String target)
            throws IOException;

    /**
     *  @param path represent the destination path
     * @throws IOException Exception if IO eror occur
     */
    void createFile(String path)
            throws IOException;

    /**
     *  @param path represent the destination path
     * @throws IOException Exception if IO eror occur
     */
    void createDirectory(String path)
            throws IOException;

    /**
     *  @param start represent the root of the disk
     *  @param filter represent the filted used
     *  @param keywords representes the keyword used
     *  @return  the list of the searh result
     * @throws IOException Exception if IO eror occur
     */
    List<Path> search(String start, SearchFilter filter, String[] keywords)
            throws IOException;
    /**
     * @throws IOException Exception if IO eror occur
     */
    void print()
            throws IOException;
}
