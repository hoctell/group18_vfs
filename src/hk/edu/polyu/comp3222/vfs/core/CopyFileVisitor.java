package hk.edu.polyu.comp3222.vfs.core;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * Created by hoctell on 3/3/2017.
 */
public class CopyFileVisitor extends SimpleFileVisitor<Path> {
    private Path fromPath;
    private Path toPath;

    /**
     * constructor
     *@param fromPath represent the source path
     *@param toPath represent the destination path
     */
    public CopyFileVisitor(Path fromPath, Path toPath) {
        this.fromPath = fromPath;
        this.toPath = toPath;
    }

    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
        Path targetPath = toPath.resolve(fromPath.relativize(dir).toString());
        Files.createDirectories(targetPath);
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        Files.copy(file, toPath.resolve(fromPath.relativize(file).toString()));
        return FileVisitResult.CONTINUE;
    }
}
