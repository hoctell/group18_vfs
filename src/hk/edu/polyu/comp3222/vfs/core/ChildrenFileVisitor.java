package hk.edu.polyu.comp3222.vfs.core;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hoctell on 31/3/2017.
 */
public class ChildrenFileVisitor extends SimpleFileVisitor<Path> {
    private List<Path> children = new ArrayList<>();
    private Path start;

    /**
     * constructor
     *@param start represent the path ofthe root
     */
    public ChildrenFileVisitor(Path start) {
        this.start = start;
    }

    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
        if (!dir.equals(start)) {
            children.add(dir);
            return FileVisitResult.SKIP_SUBTREE;
        }
        return FileVisitResult.CONTINUE;

    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        children.add(file);
        return FileVisitResult.CONTINUE;
    }

    /**
     *@return thr list of the cildren
     */
    public List<Path> getChildren() {
        return children;
    }
}
