package hk.edu.polyu.comp3222.vfs.server;

/**
 * Created by Lok on 29/3/2017.
 */

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Server class
 */
public class Server{

    private ServerSocket server;
    private final int port = 8888;

    /**
     * Constructor of server
     */
    public Server(){
        try {
            server = new ServerSocket(port);
        }catch(java.io.IOException e){
            System.out.println(e.toString());
        }
    }

    private boolean createAccount(String account, String password){
        boolean isExist = false;
        boolean success = false;
        try{
            Scanner s = new Scanner(new File("resources/server.txt"));
            List<String> list = new ArrayList<String>();
            while (s.hasNextLine()){
                list.add(s.nextLine());
            }
            s.close();
            for(String str:list){
                String[] arr = str.split(" ");
                if(arr[0].equals(account))
                    isExist = true;
            }
            if(!isExist){
                list.add(account + " " + password);
                File file = new File("resources/server.txt");
                FileWriter fw = new FileWriter(file.getAbsoluteFile(), false);
                BufferedWriter bw = new BufferedWriter(fw);
                for(String str:list) {
                    bw.write(str);
                    bw.newLine();
                }
                bw.close();
                File dir = new File("resources/Server/" +account);
                dir.mkdir();
                success = true;
            }

        }catch(IOException e){ System.out.println(e.toString()); }
        return success;
    }

    /**
     * delete the created account
     * @param account target account
     * @param password target password
     */
    public void deleteAccount(String account, String password){
        try {
            Scanner s = new Scanner(new File("resources/server.txt"));
            List<String> list = new ArrayList<String>();
            while (s.hasNextLine()) {
                list.add(s.nextLine());
            }
            s.close();
            list.remove(account+" "+password);
            File file = new File("resources/server.txt");
            FileWriter fw = new FileWriter(file.getAbsoluteFile(), false);
            BufferedWriter bw = new BufferedWriter(fw);
            for(String str:list) {
                bw.write(str);
                bw.newLine();
            }
            bw.close();
        }catch(IOException e){ System.out.println(e.toString()); }
    }
    private boolean isExist(String account,String password){
        boolean isExist = false;
        try{
            Scanner s = new Scanner(new File("resources/server.txt"));
            List<String> list = new ArrayList<String>();
            while (s.hasNextLine()){
                list.add(s.nextLine());
            }
            s.close();
            isExist = list.contains(account + " " + password);
        }catch(IOException e){ System.out.println(e.toString()); }
        return isExist;

    }

    /**
     * run the server
     */
    public void run(){
        try{
            Socket socket = server.accept();
            DataInputStream in = new DataInputStream(socket.getInputStream());
            DataOutputStream out = new DataOutputStream(socket.getOutputStream());
            BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
            String msg ="";
            String rpy ="";
            String account = "";
            final int MaxSize = 4096;
            boolean login = false;
            while(!login) {
                msg = "1. Create Account\tFormat: 1 account password\n2. Login\t\t\tFormat: 2 account password";
                out.writeUTF(msg);
                out.flush();
                rpy = in.readUTF();
                String[] arr = rpy.split(" ");
                if(arr[0].equals("1"))
                    login = createAccount(arr[1],arr[2]);
                else if(arr[0].equals("2")) {
                    login = isExist(arr[1],arr[2]);
                }
                if(login)
                    account = arr[1];
            }
            msg = "Login success!";
            out.writeUTF(msg);
            out.flush();
            DataInputStream dis = new DataInputStream(socket.getInputStream());
            while(true) {
                out.flush();
                String fn = dis.readUTF();
                System.out.println("FN: " + fn);
                if(fn.equals("Disconnect"))
                    break;
                long fs = dis.readLong();
                msg = "Staring Transfer";
                out.writeUTF(msg);
                String fp = "resources/Server" + "/" + account + "/" + fn;
                File nf = new File(fp);
                FileOutputStream fout = new FileOutputStream(nf);
                byte[] readData = new byte[MaxSize];
                int i = 0;
                int remaining = (int)fs;
                while((i = dis.read(readData,0,Math.min(readData.length,remaining))) > 0)
                {
                    remaining -= i;
                    fout.write(readData, 0, i);
                }
                fout.flush();
                fout.close();
                msg = "Finish Transfer";
                out.writeUTF(msg);
            }
            socket.close();
            System.out.println("End!");
        }catch(IOException e){System.out.println(e.toString());}
    }

    /**
     * main program for this class
     * @param args array of argruments
     */
    public static void main(String[] args){
        Server ser = new Server();
        while(true){
            ser.run();
        }
    }
}
