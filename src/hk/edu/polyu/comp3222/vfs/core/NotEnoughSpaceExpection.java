package hk.edu.polyu.comp3222.vfs.core;

/**
 * Created by hoctell on 9/4/2017.
 */
public class NotEnoughSpaceExpection extends Exception {
    /**
     * Constructor that accepts a message
     * @param message reprente the message input
     * */
    public NotEnoughSpaceExpection(String message) {
        super(message);
    }
}
