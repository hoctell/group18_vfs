package hk.edu.polyu.comp3222.vfs.test;

import hk.edu.polyu.comp3222.vfs.client.Client;
import hk.edu.polyu.comp3222.vfs.server.Server;
import org.junit.Test;

/**
 * Created by Lok on 2/4/2017.
 */
public class ServerTest {
    private Server server = new Server();

    /**
     * build the serve in a thread for the client to connect
     * @throws Exception the socket may have error
     */
    public void buildServer() throws Exception {
        try {
            (new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        server.run();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            })).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Test the login function
     */
    @Test
    public void testLogin(){
        try {
            buildServer();
            Client cli = new Client();
            cli.askForAction("C 1 loklok abc");
            cli.askForAction("1 resources/ client.vd 10000000");
            cli.askForAction("L resources/client.vd client.vd");
            cli.askForAction("2 client");
            cli.askForAction("D");
            cli.askForAction("4 list");
            buildServer();
            cli.connect("2 loklok abc");
            cli.askForAction("D");
            server.deleteAccount("loklok", "abc");
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
